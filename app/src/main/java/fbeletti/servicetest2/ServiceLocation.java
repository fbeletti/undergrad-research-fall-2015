package fbeletti.servicetest2;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * Created by fbeletti on 12/11/15.
 */
public class ServiceLocation extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    Location mCurrentLocation;
    LatLng mHomeLatLng;
    double hLat;
    double hLong;
    String mLastUpdateTime;
    String mLatitudeText;
    String mLongitudeText;
    double mDistance;
    int mHome;
    double RADIUS_DISTANCE;
    long REQUEST_INTERVAL = 5 * 60 * 1000;

    Boolean mRequestingLocationUpdates;

    MySQLiteHelper db;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate(){
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(REQUEST_INTERVAL);
        //mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        //Home position hard coded as it is intended to come from the main application
        hLat = 41.701856;
        hLong = -86.238002;
        mHomeLatLng = new LatLng(hLat, hLong);

        RADIUS_DISTANCE = 0.025;

        mRequestingLocationUpdates = true;

        db = new MySQLiteHelper(this);

    }

    @Override
    public void onDestroy(){
        stopLocationUpdates();
        if(mGoogleApiClient.isConnected()){
            mGoogleApiClient.disconnect();
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onStart(Intent intent, int startid){
        mGoogleApiClient.connect();
        if(mGoogleApiClient.isConnected() && !mRequestingLocationUpdates){
            startLocationUpdates();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            mLatitudeText = String.valueOf(mLastLocation.getLatitude());
            mLongitudeText = String.valueOf(mLastLocation.getLongitude());
        }

        if(mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        updateUI();
    }

    private void updateUI() {
        String curLatitude;
        String curLongitude;
        LatLng curLocation;
        double home_dist;
        String oldLatitude;
        String oldLongitude;
        LatLng oldLatLng = null;

        //getting new location
        curLatitude = new DecimalFormat("#.####").format(mCurrentLocation.getLatitude()).replaceAll(",", ".");
        curLongitude = new DecimalFormat("#.####").format(mCurrentLocation.getLongitude()).replaceAll(",", ".");
        curLocation = new LatLng(Double.parseDouble(curLatitude), Double.parseDouble(curLongitude));

        //getting old location
        String ans = db.getLastLocation();
        if (!ans.contentEquals("0")){
            String[] parts = ans.split(" ");
            oldLatitude = parts[0];
            oldLongitude = parts[1];
            oldLatLng = new LatLng(Double.parseDouble(oldLatitude), Double.parseDouble(oldLongitude));
        }

        //calculating distance from home point
        home_dist = getDistanceInKm(curLocation, mHomeLatLng);
        if (home_dist < RADIUS_DISTANCE) {
            mHome = 1;
        }
        else {
            mHome = 0;
        }

        //calculating distance from last point
        if(oldLatLng != null) {
            mDistance = getDistanceInKm(curLocation, oldLatLng);
        }
        else {
            mDistance = 0;
        }

        db.addLocation(mCurrentLocation, mLastUpdateTime, mDistance, mHome);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public double toRadians(Double degree){
        Double radians;
        radians = (degree * Math.PI)/180;
        return radians;
    }

    public double getDistanceInKm(LatLng pos1, LatLng pos2){
        Double R = 6371.0; // km
        Double v1 = toRadians(pos1.latitude);
        Double v2 = toRadians(pos2.latitude);
        Double d1 = toRadians(pos2.latitude-pos1.latitude);
        Double d2 = toRadians(pos2.longitude-pos1.longitude);

        Double a = Math.sin(d1/2) * Math.sin(d1/2) +
                Math.cos(v1) * Math.cos(v2) *
                        Math.sin(d2/2) * Math.sin(d2/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        Double d = R * c;
        return d;
    }
}
