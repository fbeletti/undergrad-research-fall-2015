package fbeletti.servicetest2;

import android.bluetooth.BluetoothDevice;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.text.DateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by fbeletti on 12/11/15.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "SensorsDB";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create location table

        String CREATE_LOCATION_TABLE = "CREATE TABLE location ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "latitude REAL, "+
                "longitude REAL," +
                "locationchange_timestamp TEXT," +
                "distance_last REAL," +
                "flag_home INTEGER )";

        // create tables
        db.execSQL(CREATE_LOCATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older tables if existed
        db.execSQL("DROP TABLE IF EXISTS location");

        // create fresh table
        this.onCreate(db);
    }

    //CRUD
    // Location table name
    private static final String TABLE_LOCATION = "location";

    //Location Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_LOCATIONCHANGE_TIMESTAMP = "locationchange_timestamp";
    private static final String KEY_DISTANCE_LAST = "distance_last";
    private static final String KEY_FLAG_HOME = "flag_home";

    private static final String[] LOCATION_COLUMNS = {KEY_ID,KEY_LATITUDE,KEY_LONGITUDE, KEY_LOCATIONCHANGE_TIMESTAMP, KEY_DISTANCE_LAST, KEY_FLAG_HOME};


    public void addLocation(Location location, String timeStamp, double distance, int flag){
        //Log.d("addLocation", location.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_LATITUDE, location.getLatitude());
        values.put(KEY_LONGITUDE, location.getLongitude());
        values.put(KEY_LOCATIONCHANGE_TIMESTAMP, timeStamp);
        values.put(KEY_DISTANCE_LAST, distance);
        values.put(KEY_FLAG_HOME, flag);

        // 3. insert
        db.insert(TABLE_LOCATION, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public void getAllLocations() {
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_LOCATION;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
         if (cursor.moveToFirst()) {
            do {
                String line = "[id=" + cursor.getString(0) + ", latitude=" + cursor.getString(1) +
                        ", longitude=" + cursor.getString(2) + ", timestamp=" + cursor.getString(3) +
                        ", distance_last=" + cursor.getString(4) + ", flag=" + cursor.getString(5) +
                        "]";

                Log.d ("getAllLocations()", line);
            } while (cursor.moveToNext());
        }

    }

    public String getLastLocation() {
        String line = "0";
        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_LOCATION;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        if (cursor.moveToLast()) {
            line = cursor.getString(1) + " " + cursor.getString(2);
        }

        return line;
    }
}
