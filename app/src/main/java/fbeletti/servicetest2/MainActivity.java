package fbeletti.servicetest2;

import android.content.Intent;
import android.location.Location;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.text.StringCharacterIterator;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener{

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    TextView mLatitudeText;
    TextView mLongitudeText;
    TextView mLastUpdateTimeText;
    LocationRequest mLocationRequest;
    Location mCurrentLocation;
    String mLastUpdateTime;
    Boolean mRequestingLocationUpdates = true;

    Button button;
    Button button2;

    String LAST_UPDATED_TIME_STRING_KEY = "1";
    String LOCATION_KEY = "2";
    String REQUESTING_LOCATION_UPDATES_KEY = "3";

    MySQLiteHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        button.setOnClickListener(this);
        button2.setOnClickListener(this);

        db = new MySQLiteHelper(this);

        //db.getAllLocations();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_LONG).show();
                startService(new Intent(this, ServiceLocation.class));
                break;
            case R.id.button2:
                stopService(new Intent(this, ServiceLocation.class));
                Toast.makeText(getApplicationContext(), "Service stopped", Toast.LENGTH_LONG).show();
                break;
        }
    }
}

